%include "lib.inc"

section .text
%define KEY_SHIFT 8

; Принимает два аргумента:
; rdi - указатель на нуль-терминированную строку.
; rsi - указатель на начало словаря.
global find_word
find_word:
    push rbx
    .loop:
        mov rbx, rsi
        add rsi, KEY_SHIFT
        push rdi
        call string_equals
        pop rdi
        cmp rax, TRUE
        je .success
        cmp qword[rbx], 0
        je .not_found
        mov rsi, [rbx]
        jmp .loop

    .not_found:
        xor rax, rax
        jmp .end
    
    .success:
        mov rax, rbx
        jmp .end

    .end:
        pop rbx
        ret
