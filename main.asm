%include "dict.inc"
%include "words.inc"
%include "lib.inc"


section .data
    string: times 256 db 0

section .rodata
    INPUT_ERROR_MSG: db "Incorrect input", 0 
    NOT_FOUND_MSG: db "Key is not in map", 0

section .text
    global _start
    _start:
        mov rdi, string
        mov rsi, 255
        call read_string
        test rax, rax
        jle .input_error

        mov rdi, string
        mov rsi, head
        call find_word
        test rax, rax
        je .not_found
        
        add rax, KEY_SHIFT
        push rax
        mov rdi, rax
        call string_length
        pop rcx
        add rax, rcx
        lea rdi, [rax + 1]
        call print_string

        xor rdi, rdi
        jmp exit

        .not_found:
            mov rdi, NOT_FOUND_MSG
            call print_error_string
            mov rdi, 1
            jmp exit

        .input_error:
            mov rdi, INPUT_ERROR_MSG
            call print_error_string
            mov rdi, 1
            jmp exit

