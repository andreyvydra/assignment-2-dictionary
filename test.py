import subprocess
import os

test_data = (
    ("first", "first word", 0),
    ("second", "second word", 0),
    ("third", "third word", 0),
    ("first\n", "Key is not in map", 1),
    ("""fdgfdgsdfgfdgfdgfdsgsdfgfdgfdgfdsg" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff" +
     "sdfgfdgdfgfdsgdfsgfdgdfsgdfgfffffff""", "Key is not in map", 1),
)

subprocess.run(["make", "clean"], stdout=open(os.devnull, 'wb'))
subprocess.run(["make", "build"], stdout=open(os.devnull, 'wb'))
for idx, test in enumerate(test_data):
    inp, expected_output, expected_code = test
    print(idx, end=" ")
    result = subprocess.run(["./prog"], input=inp.encode("ascii"), capture_output=True)
    output_line = result.stdout.decode("ascii")
    output_err_line = result.stderr.decode("ascii")
    assert ((expected_code == result.returncode == 1 and output_err_line == expected_output) or 
            (expected_code == result.returncode == 0 and output_line == expected_output))
    print("OK")
subprocess.run(["make", "clean"], stdout=open(os.devnull, 'wb'))
print("All tests are passed!")