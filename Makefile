ASM=nasm
FLAGS=-felf64 -g

%.o: %.asm %.inc
	$(ASM) $(FLAGS) $< -o $@

.PHONY: build
build: main.o lib.o dict.o
	ld -o prog main.o lib.o dict.o

.PHONY: test
test:
	python3 test.py

.PHONY: clean
clean: 
	rm -rf *.o prog
